import 'dart:io';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:project_sq/model/menu_model.dart';

class DatabaseMenu {
  DatabaseMenu._privateConstructor();
  static final DatabaseMenu instance = DatabaseMenu._privateConstructor();

  static Database? _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'db_profile.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE profileDB (
          id INTEGER PRIMARY KEY,
          name TEXT,
          price TEXT,
          quantity TEXT,
          image TEXT
      )
      ''');
  }

  Future<List<MenuModel>> getProfiles() async {
    Database db = await instance.database;
    var profiles = await db.query('profileDB', orderBy: 'name');
    List<MenuModel> groceryList = profiles.isNotEmpty
        ? profiles.map((c) => MenuModel.fromMap(c)).toList()
        : [];
    return groceryList;
  }

  Future<List<MenuModel>> getProfile(int id) async{
    Database db = await instance.database;
    var profile = await db.query('profileDB', where: 'id = ?', whereArgs: [id]);
    List<MenuModel> profileList = profile.isNotEmpty
        ? profile.map((c) => MenuModel.fromMap(c)).toList()
        : [];
    return profileList;
  }

  Future<int> add(MenuModel profile) async {
    Database db = await instance.database;
    return await db.insert('profileDB', profile.toMap());
  }

  Future<int> remove(int id) async {
    Database db = await instance.database;
    return await db.delete('profileDB', where: 'id = ?', whereArgs: [id]);
  }

  Future<int> update(MenuModel profile) async {
    Database db = await instance.database;
    return await db.update('profileDB', profile.toMap(),
        where: "id = ?", whereArgs: [profile.id]);
  }
}