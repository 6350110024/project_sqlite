
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:project_sq/database/database_menu.dart';
import 'package:project_sq/model/menu_model.dart';

class ShowMenu extends StatelessWidget {
  final id;
  ShowMenu({Key? key, required this.id}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     backgroundColor: Colors.brown[100],
      body:
      Center(
        child: FutureBuilder<List<MenuModel>>(
            future: DatabaseMenu.instance.getProfile(this.id),
            builder: (BuildContext context,
                AsyncSnapshot<List<MenuModel>> snapshot) {
              if (!snapshot.hasData) {
                return Center(child: Text('Loading...'));
              }
              return snapshot.data!.isEmpty
                  ? Center(child: Text('No Groceries in List.'))
                  : ListView(
                children: snapshot.data!.map((profile) {
                  return Center(
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 130),
                          child: Image.asset("assets/images/detail.png", width: 300),
                        ),
                        CircleAvatar(
                            backgroundImage: FileImage(File(profile.image)),
                            radius: 100,
                          ),
                        Padding(
                          padding: const EdgeInsets.only(top: 20),
                          child: Text('Name : ${profile.name}',
                            style:TextStyle(
                            fontSize: 20,
                          ) ,),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 7),
                          child: Text('Price : ${profile.price} Bath',
                            style:TextStyle(
                              fontSize: 20,
                            ) ,),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 7),
                          child: Text('quantity : ${profile.quantity}',
                            style:TextStyle(
                              fontSize: 20,
                            ) ,),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 30),
                          child: SizedBox(
                            width: 100,
                            height: 60,
                            child: ElevatedButton(
                              style: ButtonStyle(
                                  backgroundColor: MaterialStateProperty.all(Colors.brown),
                                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                      RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(40.0),
                                      ))),
                              child: Text(
                                "Back",
                                style: TextStyle(fontSize: 22, color: Colors.black),
                              ),
                              onPressed: () {
                                Navigator.pop(context,"...");
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }).toList(),
              );
            }),
      ),
    );
  }
}
