import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:project_sq/pages/about.dart';
import 'package:project_sq/pages/list.dart';
import 'package:project_sq/pages/recommened.dart';

class Homepage extends StatelessWidget {
  const Homepage({Key? key, required String title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown[400],
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [

             const Padding(
           padding: EdgeInsets.only(left: 10 ,bottom: 30, top: 75,),
           child: CircleAvatar(
           backgroundImage: AssetImage("image/1.jpg"),
           radius: 150 ,

             ),
      ),

            SizedBox(
              width: 100,
              height: 50,
              child: ElevatedButton(
                style: ButtonStyle(
                    backgroundColor:
                    MaterialStateProperty.all(Colors.grey[900]),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(40.0),
                        ))),
                child: Text(
                  "เมนู",
                  style: TextStyle(fontSize: 22, color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(
                      builder: (context){
                        return ListMenu(title: 'Profile Lists');
                      })
                  );
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: SizedBox(
                width: 150,
                height: 80,
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all(Colors.black12),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40.0),
                          ))),
                  child: Text(
                    "เครื่องดื่ม",
                    style: TextStyle(fontSize: 22, color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return RecommenedMenu();
                        })
                    );
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: SizedBox(
                width: 100,
                height: 60,
                child: ElevatedButton(
                  style: ButtonStyle(
                      backgroundColor:
                      MaterialStateProperty.all(Colors.grey[700]),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(40.0),
                          ))),
                  child: Text(
                    "ติดต่อ",
                    style: TextStyle(fontSize: 22, color: Colors.white),
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(
                        builder: (context){
                          return About();
                        })
                    );
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 80, top: 30),
              child: Row(
                children: [
                  Icon(
                    Icons.local_cafe,
                    color: Colors.black,
                    size: 50,
                  ),
                  Text(
                    " :  coffee_shop",
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 80, top: 5),
              child: Row(
                children: [

                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
