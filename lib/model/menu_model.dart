
class MenuModel {
  int? id;
  String name;
  String price;
  String quantity;
  String image;

  MenuModel({
    this.id,
    required this.name,
    required this.price,
    required this.quantity,
    required this.image,

  });

  factory MenuModel.fromMap(Map<String, dynamic> json) =>
      new MenuModel(
        id: json['id'],
        name: json['name'],
        price: json['price'],
        quantity: json['quantity'],
        image: json['image'],
      );

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'name': name,
      'price': price,
      'quantity': quantity,
      'image': image,
    };
  }
}